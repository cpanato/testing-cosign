# Example of using Cosign to sign an image using GitLab Pipeline

This Example shows how to use GitLab JWT Token to sign container or blobs using sigstore cosign for both
staging and production sigstore infrastructure.
